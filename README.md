# README #

### What is this repository for? ###

ForestLAS is Python code for reading, writing and analysing point data in the .LAS format. Essentially, this reads LiDAR data into a numpy array which opens up the wide world of Python to analysing point data.  Reading of ALS data is done with the lasIO module. The code was originally based on pyLAS which I think is no longer maintained (http://pylas.sourceforge.net/).
  
The original project intention was the analysis of ALS data for forestry 
applications, however there could be other potential users.  Data can be easily extracted (incl. round plots) and gridded etc. using the lasIO module. Also rasters of ALS metrics can be generated using the woodyAttribute module, this utilises multiprocessing to speed up production. The canopyComplexity module is designed to generalise the canopy height profile (Wilkes et al. in prep).

If you want to give this a try it is probably best you contact me so I can get you started.  

Version: still very early! If you do use this code please cite me.

### How do I get set up? ###

There is a setup.py file for easy installation.

Requires numpy and scipy. Some modules require gdal.

### Contribution guidelines ###

Drop me a line

### Who do I talk to? ###

phil.wilkes@rmit.edu.au